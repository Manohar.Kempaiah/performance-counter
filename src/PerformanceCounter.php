<?php

namespace ClarkeTech\PerformanceCounter;

/**
 * Calculates the average iteration time for a given process
 *
 * Keys can be nested which enables you to measure the performance of inner and outer loops.
 * Designed to be used as a development utility tool. Recommended to be removed from code after use.
 *
 * @see PerformanceCounterTest::average_process_time_can_be_obtained_for_multiple_keys for a demo
 * of how this works
 *
 * @author Gary Clarke <info@garyclarke.tech>
 */
final class PerformanceCounter
{
    public function displayString() {
        echo "This is a string.";
    }
    public function displayCurrentDate() {
        // Get the current date and time
        // $currentDate = date('Y-m-d H:i:s');
        
        // Display the current date
        echo "Current Date and Time: ",date('Y-m-d H:i:s');
    }
}